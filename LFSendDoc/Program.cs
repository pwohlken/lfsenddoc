﻿using System;
using System.IO;
using System.Collections;
using Microsoft.Win32;
using Laserfiche.RepositoryAccess;
using Laserfiche.DocumentServices;

namespace LFSendDoc
{
    class Program
    {
        static string GetMimeType(FileInfo fileInfo)
        {
            string mimeType = "application/unknown";

            RegistryKey regKey = Registry.ClassesRoot.OpenSubKey(fileInfo.Extension.ToLower());

            if (regKey != null)
            {
                object contentType = regKey.GetValue("Content Type");

                if (contentType != null)
                {
                    mimeType = contentType.ToString();
                }
            }

            return mimeType;
        }

        static int Main(string[] args)
        {
            //get command-line arguments
            //app expects five arguments:  
            //
            //Windows folder containing file, e.g. "\\allcity\amanda\amandaattachments"
            //Laserfiche repository, e.g "CityOfSalem"
            //Laserfice folder to store the document, e.g. "Planning\Planning\Code Interpretation"
            //Laserfiche template of the document, e.g. "Planning"
            //Laserfiche metadata, a pipe-delimited list of field name and value pairs, e.g. "Case Number|20-01|Case Name|Land Use Verification"
            
            int EntryID = 0;

            if (args.Length < 6)
                return -1;

            string win_path = args[0];
            string repository = args[1];
            string lf_path = args[2];
            string lf_name = args[3];
            string template = args[4];
            string metadata = args[5];

            string[] metadata_fields = metadata.Split(new Char [] {'|'});

            if ( ( metadata_fields.Length > 1 ) && ( ( metadata_fields.Length % 2 ) != 0 ) )
                return -2; 

            Server LFServer = new Server("laserfiche");
            Session LFSession = new Session();
            RepositoryRegistration LFRepositoryReg = new RepositoryRegistration(LFServer, repository);

            try
            {
                LFSession.LogIn(LFRepositoryReg);

                string sPath = string.Empty;
                string sVol = string.Empty;
                string file_name = string.Empty;
                bool is_tiff = false;
                EntryInfo eitemp;
                FileInfo infotemp = new FileInfo(win_path);

                //file does not exist, so return
                if (!infotemp.Exists)
                    return -3;

                //file is empty, so return
                if (infotemp.Length == 0)
                    return -4;

                is_tiff = (infotemp.Extension.ToLower().EndsWith("tif") || infotemp.Extension.ToLower().EndsWith("tiff"));
                   
                if (lf_name == "NONE")
                    file_name = infotemp.Name;
                else
                    if (lf_name.Contains(".") || is_tiff)
                        file_name = lf_name;
                    else
                        if (infotemp.Extension.Length > 0)
                            file_name = lf_name + infotemp.Extension;
                        else
                            file_name = lf_name;

                ArrayList lf_intermediate_folders = new ArrayList(lf_path.Split('\\'));

                //Check for intermediate folders and create if necessary
                if (lf_intermediate_folders.Count > 0)
                {
                    for (int i = 0; i < lf_intermediate_folders.Count; i++)
                    {
                        try
                        {
                            sPath = sPath + "\\" + lf_intermediate_folders[i];
                            eitemp = Entry.GetEntryInfo(sPath, LFSession);

                            if (i == 0)
                            {
                                sVol = eitemp.VolumeName;
                            }

                        }
                        catch (LaserficheRepositoryException ex)
                        {
                            if (ex.ErrorCode == 9001) //"Entry not found"
                            {
                                Folder.Create(sPath, sVol, EntryNameOption.None, LFSession);
                            }
                        }
                    }
                }

                FieldValueCollection fvc = new FieldValueCollection();
                for (int i = 0; i < metadata_fields.Length - 1; i++)
                {
                    bool is_date = DateTime.TryParse(metadata_fields[i + 1], out DateTime dDate);
                    if (is_date)
                        fvc.Add(metadata_fields[i], dDate);
                    else
                        fvc.Add(metadata_fields[i], metadata_fields[i + 1]);
                    i++;
                }

                //Create document
                try
                {
                    using (DocumentInfo docinfo = new DocumentInfo(LFSession))
                    {
                        docinfo.Create(lf_path + "\\" + file_name, sVol, EntryNameOption.AutoRename);
                        DocumentImporter di = new DocumentImporter();
                        di.Document = docinfo;
                        docinfo.SetTemplate(template, fvc);
                        docinfo.Save();

                        if (is_tiff)
                        {
                            try
                            {
                                di.ImportImages(infotemp.FullName);
                            }
                            catch (Exception ex)
                            {
                                // tif extension but not readable by Laserfiche.  Import as electronic file.
                                if (ex.Message == "Lfi_GetFileInfo failed.")
                                {
                                    di.ImportEdoc("application/unknown", infotemp.FullName);
                                }
                                else
                                    throw ex;
                            }
                        }
                        else
                        {
                            di.ImportEdoc(GetMimeType(infotemp), infotemp.FullName);
                        }

                        EntryID = docinfo.Id;
                    }
                }
                finally
                {
                }
            }
            catch (LaserficheRepositoryException ex)
            {
                return ex.ErrorCode * -1;
            }
            finally
            {
                //close the Laserfiche session
                if (LFSession != null)
                    LFSession.Close();
            }

            return EntryID;
        }
    }
}
